import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  @ViewChild('contract') mymodal!: ElementRef;
  constructor(private modalService: NgbModal, private http: HttpClient) { }
  closeResult: string = "";
  public toggle:string = "none";
  public toggleText:string = "Preview";
  public name:string = "Montasir";
  public email:string = "Montasir";
  public phone:string = "Montasir";
  public address1:string = "Montasir";
  public address2:string = "Montasir";
  public contract:string = "";
  ngOnInit(): void {
  }
  changeName(e:any) {
    this.name = e.target.value;
  }
  changeEmail(e:any) {
    this.email = e.target.value;
  }
  changePhone(e:any) {
    this.phone = e.target.value;
  }
  changeAd(e:any) {
    this.address1 = e.target.value;
  }
  changeAdt(e:any) {
    this.address2 = e.target.value;
  }

  open(content: any) {
    this.contract = `${this.name}`;
    this.http.get('assets/template/test.html', {responseType: "text"}).subscribe(data => {
      let buffer = data.replace("Vname", this.name);
      buffer = buffer.replace("Vemail", this.email);
      buffer = buffer.replace("Vphone", this.phone);
      buffer = buffer.replace("Vaddress1", this.address1);
      buffer = buffer.replace("Vaddress2", this.address2);
      this.contract = buffer;
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    console.log(this.closeResult);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  exportAsPDF()
  {
    let data = document.getElementById("contract")!;
    html2canvas(data).then(canvas => {
      const contentDataURL = canvas.toDataURL('image/png');
      var pdf = new jsPDF('p', 'pt', [canvas.width+500, canvas.height]);
      // let pdf = new jspdf('p', 'cm', 'a4'); Generates PDF in portrait mode
      pdf.addImage(contentDataURL,0,0,canvas.width, canvas.height);
      pdf.save('contract.pdf');
    });
  }

  public convetToPDF()
  {
    let data = document.getElementById('contract')!;
    html2canvas(data).then(canvas => {
      let imgWidth = 208;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      pdf.addImage(contentDataURL, 'PNG', 0, 0, imgWidth, imgHeight)
      pdf.save('new-file.pdf'); // Generated PDF
    });
  }

}
