import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.css']
})
export class FormTemplateComponent implements OnInit {
  constructor(
    private route: ActivatedRoute
  ) {}
  emailParam: string = "";
  ngOnInit() {
    this.emailParam = this.route.snapshot.queryParamMap.get('email')!;
  }

}
